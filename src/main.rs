#![allow(unused_parens)]

use std::io::{self, Write};

fn main() {
    println!("Welcome to GGLinnk's Rust Temperature Converter.");
    println!("");
    println!("(1) : Celsius to Fahrenheit.");
    println!("(2) : Fahrenheit to Celsius.");
    //println!("(3) : Celsius to Kelvin.");
    //println!("(4) : Kelvin to Celsius.");
    //println!("(5) : Fahrenheit to Kelvin.");
    //println!("(6) : Kelvin to Fahrenheit.");
    println!("");

    let choice: i32 = get_user_input();
    println!("Number : {}", choice);
    match choice {
        1 => celsius_to_fahrenheit(),
        2 => fahrenheit_to_celsius(),
        //3 => celsius_to_kelvin(),
        //4 => kelvin_to_celsius(),
        //5 => fahrenheit_to_kelvin(),
        //6 => kelvin_to_fahrenheit(),
        _ => println!("Invalid choice !"),
    }
}

fn celsius_to_fahrenheit() {
    println!("Celsius to Fahrenheit convertor.");
    loop {
        let mut input_temp = String::new();

        print!("Enter temperature you want to convert : ");

        io::stdout().flush().unwrap();

        io::stdin()
            .read_line(&mut input_temp)
            .expect("Warning : Unable to read line !");

        let input_temp: f32 = match input_temp.trim().parse() {
            Ok(number) => number,
            Err(_) => continue,
        };
        let output_temp = (input_temp * 9.0 / 5.0) + 32.0;
        println!("{}° Celsius is {}° Fahrenheit", input_temp, output_temp);
        break;
    }
}

fn fahrenheit_to_celsius() {
    println!("Fahrenheit to Celsius convertor.");
    loop {
        let mut input_temp = String::new();

        print!("Enter temperature you want to convert : ");

        io::stdout().flush().unwrap();

        io::stdin()
            .read_line(&mut input_temp)
            .expect("Warning : Unable to read line !");

        let input_temp: f32 = match input_temp.trim().parse() {
            Ok(number) => number,
            Err(_) => continue,
        };
        let output_temp = (input_temp - 32.0) * 5.0 / 9.0;
        println!("{}° Fahrenheit is {}° Celsius", input_temp, output_temp);
        break;
    }
}

fn get_user_input() -> i32 {
    return loop {
        let mut user_choice = String::new();

        print!("Select what conversion you want to do : ");

        io::stdout().flush().unwrap();

        io::stdin()
            .read_line(&mut user_choice)
            .expect("Warning : Unable to read line !");

        match user_choice.trim().parse() {
            Ok(number) => break number,
            Err(_) => continue,
        };
    };
}
